import { AbstractModel } from './abstract-model';

export class Avance extends AbstractModel {

  private _avance: string;

  constructor(id?: number, avance?: string) {
    super(id);
    this._avance = avance;
  }

  /**
   * Getters and Setters ----------------------------------------------------
   */

  public get avance(): string {
    return this._avance;
  }

  public set avance(value: string) {
    this._avance = value;
  }

}
