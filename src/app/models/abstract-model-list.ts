import { AbstractModel } from './abstract-model';

export class AbstractModelList<AbstractModel> {

  private _objectList: Array<AbstractModel>;
  private _pagination: number;

  constructor(objectList: Array<AbstractModel>, pagination: number) {
    this._objectList = objectList;
    this._pagination = pagination;
  }

  /**
   * Getters and Setters ----------------------------------------------------
   */

  public get objectList(): Array<AbstractModel> {
    return this._objectList;
  }

  public set objectList(value: Array<AbstractModel>) {
    this._objectList = value;
  }

  public get pagination(): number {
    return this._pagination;
  }

  public set pagination(value: number) {
    this._pagination = value;
  }
}
