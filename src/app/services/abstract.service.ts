import { AbstractModel } from '../models/abstract-model';
import { AbstractModelList } from '../models/abstract-model-list';

export abstract class AbstractService<T extends AbstractModel> {

  /**
   * Object serializer. Returns a typed element parsed from a JSON string
   *
   * @abstract
   * @param {*} object Element unique identifier
   * @returns {T} Serialized element
   * @memberof AbstractService
   */
  public abstract toObject(object: any): T;

  /**
   * Array of Objects serializer. Returns an array of  typed elements parsed
   * from a JSON string
   *
   * @abstract
   * @param {*} object Element unique identifier
   * @returns {Array<T>} Array of Serialized elements
   * @memberof AbstractService
   */
  public abstract toObjectArray(object: any): Array<T>;

  /**
   * Returns a element from the server. This method parses the element
   * and returns a typed element
   *
   * @param {number} id Element unique identifier
   * @returns {T} Serialized element
   * @memberof AbstractService
   */
  public get(id: number): T {
    // Mocked response from the server
    const response: any = JSON.parse(`{"id":"1", "avance":"Hello World!"}`);
    return this.toObject(response);
  }

  /**
   * Returns a list of elements from the server. This method parses the elements
   * and returns an object with and array of typed elements and a pagination number
   *
   * @returns {AbstractModelList<T>} List of elements with pagiantion number
   * @memberof AbstractService
   */
  public getAll(): AbstractModelList<T> {
    // Mocked response from the server
    const response: any = JSON.parse(`[{"id":"1", "avance":"Avance 1"},{"id":"2", "avance":"Avance 2"}]`);
    const pagination: number = 5;
    return new AbstractModelList(this.toObjectArray(response), pagination);
  }
}
