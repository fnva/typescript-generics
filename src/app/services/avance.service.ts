import { Avance } from '../models/avance';
import { AbstractService } from './abstract.service';

export class AvanceService extends AbstractService<Avance> {

  public toObject(object: any): Avance {
    const result: Avance = new Avance();
    if (object != null) {
      // Parse all elements from the model
      result.id = object.id;
      result.avance = object.avance;
    }
    return result;
  }

  public toObjectArray(object: any): Array<Avance> {
    const resultArray: Array<Avance> = new Array<Avance>();
    if (object != null) {
      // Iterate through all the elements in the array
      for (let i = 0; i < object.length; i++) {
        resultArray.push(this.toObject(object[i]));
      }
    }
    return resultArray;
  }
}
