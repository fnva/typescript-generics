import { Component, OnInit } from '@angular/core';

import { Avance } from './models/avance';
import { AvanceService } from './services/avance.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {

  public title: string = 'app';
  public avance: Avance;
  public avances: Array<Avance>;
  public pagination: number;

  public constructor(private avanceService: AvanceService) {
  }

  ngOnInit() {

    this.avance = this.avanceService.get(1);
    this.avances = this.avanceService.getAll().objectList;
    this.pagination = this.avanceService.getAll().pagination;

  }
}
